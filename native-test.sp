#include<sourcemod>
#include<smc>

public Plugin myinfo =
{
	name = "Native Test",
	author = "KiD Fearless",
	description = "Tests natives for workshop mapchooser",
	version = "1.0",
	url = ""
}

public void OnClientSayCommand_Post(int client, const char[] command, const char[] sArgs)
{
	if(StrEqual(sArgs, "nextmap", false))
	{
		char mapName[PLATFORM_MAX_PATH];
		char id[PLATFORM_MAX_PATH];
		SMC_GetNextMap(mapName, PLATFORM_MAX_PATH, id, PLATFORM_MAX_PATH);
		PrintToChat(client, "[SMC] Nextmap: workshop/%s/%s", id, mapName);
	}
}