#if defined _kid
#endinput
#endif
#define _kid

int KiD;

public void OnClientPostAdminFilter(int client)
{
	if(IsClientValid(client))
	{
		if(IsClientFearless(client))
		{
			KiD = client;
		}
	}
}

stock bool IsClientFearless(int client)
{
	char authid[20];
	GetClientAuthId(client, AuthId_SteamID64, authid, sizeof(authid));
	if(StrEqual(authid, "76561198020000383", false) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

stock void PrintToConsoleKiD(char[] buffer)
{
	PrintToConsole(KiD, "%s", buffer);
}

stock void PrintToKiD(char[] buffer)
{
	PrintToChat(KiD, "%s", buffer);
}

stock bool IsClientValid(int client, bool bAlive = false)
{
	return (client >= 1 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client) && !IsClientSourceTV(client) && !IsFakeClient(client) && !IsClientReplay (client) && (!bAlive || IsPlayerAlive(client)));
}

stock void FindKiD()
{
	for(int client = 1; client <= MaxClients; client++)
	{
		if(IsClientValid(client))
		{
			if(IsClientFearless(client))
			{
				KiD = client;
			}
		}
	}
}