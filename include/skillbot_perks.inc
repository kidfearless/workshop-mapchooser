#if defined _skillbot_perks_included_
  #endinput
#endif
#define _skillbot_perks_included_

#define ITEM_NAME_LENGTH 32
#define ITEM_HANDLER_LENGTH 32

#define PERKS_MAX_HANDLERS 32
#define PERKS_MAX_ITEMS 512

char SectionID[] = "|";

#define STRING(%1) %1, sizeof(%1)

enum Perk_Type
{
	String:szType[ITEM_HANDLER_LENGTH],
	Handle:hCookie,
	Handle:hPlugin,
	Function:fnMapStart,
	Function:fnReset,
	Function:fnConfig,
	Function:fnUse,
	Function:fnRemove,
	useCookies
}

enum Perk_Item
{
	String:szName[ITEM_NAME_LENGTH],
	iType,
	iRank,
	iParent,
	iFlagBits
}

native Perks_RegisterHandler(String:type[ITEM_HANDLER_LENGTH], Handle:cookie, Function:mapstart, Function:reset, Function:config, Function:use, Function:remove, useCookies);
native Perks_Reequip(client, String:type[ITEM_HANDLER_LENGTH]);
native Perks_ToggleTracers(client);

public SharedPlugin __pl_skillbotperks =
{
	name = "skillbot_perks",
	file = "skillbot_perks.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};
