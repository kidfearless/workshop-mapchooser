/**
 * File: War3Source_UltDamage.inc
 * Description: Stocks regarding anything that does fire damage
 * Author(s): thorgot
 */

#if defined __war3source_firedamage__
#endinput
#endif
#define __war3source_firedamage__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================


native bool:W3DoFireDamage(attacker,victim,dmg_per_second,number_of_hits, Float:time_per_hit, String:weaponname[]);

//Checks if the victim is currently on fire via the attacker
native bool:IsBurning(attacker,victim);