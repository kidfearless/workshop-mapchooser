/**
 * File: War3Source_SkillEffects.inc
 * Description: Provide effects for certain skills so they feel the same across all races
 * Author(s): War3Source Team  
 */

/** 
 * Evade incoming damage.
 * 
 * Use inside OnW3TakeDmgAllPre or OnW3TakeDmgBulletPre if the victim
 * should evade the incoming damage.
 */
native War3_EvadeDamage(victim, attacker);

/** 
 * Stock for skills that return damage, like Thorns Aura.
 * 
 * Doesn't actually return any damage, only does the effect :)
 * 
 * Note that victim is the guy getting shot and attacker the guy
 * who takes return damage.
 */
native War3_EffectReturnDamage(victim, attacker, damage, skill);

/** 
 * Stock for skills that leech HP
 * 
 * Currently does not show any effect for the victim
 */
native War3_VampirismEffect(victim, attacker, leechhealth);

/**
 * Called after health was leeched from a player
 */
forward OnWar3VampirismPost(victim, attacker, iHealthLeeched);

/** 
 * Stock for skills that bash
 */
native War3_BashEffect(victim, attacker);

/**
 * Stock for wards
 */
native War3_WardVisualEffect(wardindex, beamcolor[4]);


//uses the stock above to do hud msgs easier, set string to "" to remove hud msgs
//conveniently doesn't reset constantly like if you use the stock (has a refresh timer)
native W3SetHudMsg(client,channel,const String:sHudMsgText[], const Float:fPos[2],const color[4]);

new UserMsg:g_HudMsg;
stock W3HudMsg(iClient, iChannel, const Float:fPosition[2], const iColor1[4], const iColor2[4], iEffect, Float:fFadeInTime, Float:fFadeOutTime, Float:fHoldTime, Float:fEffectTime, const String:szText[], any:...)
{
    if(GetUserMessageType() != UM_Protobuf)
        return false;
    g_HudMsg = GetUserMessageId("HudMsg");
    decl String:szBuffer[256];
    VFormat(szBuffer, sizeof(szBuffer), szText, 12);
    
    decl iClients[1];
    iClients[0] = iClient;
    
    new Handle:hMessage = StartMessageEx(g_HudMsg, iClients, 1);
    PbSetColor(hMessage, "clr2", iColor2);
    PbSetInt(hMessage, "effect", iEffect);
    PbSetFloat(hMessage, "hold_time", fHoldTime);
    PbSetFloat(hMessage, "fx_time", fEffectTime);
    PbSetString(hMessage, "text", szBuffer);
    PbSetFloat(hMessage, "fade_in_time", fFadeInTime);
    PbSetFloat(hMessage, "fade_out_time", fFadeOutTime);
    PbSetInt(hMessage, "channel", iChannel);
    PbSetVector2D(hMessage, "pos", fPosition);
    PbSetColor(hMessage, "clr1", iColor1);
    EndMessage();
    
    return true;
}