/**
 * File: War3Source_Statistics.inc
 * Description: Stocks regarding Statistics (espeically bug reports)
 * Author(s): thorgot  
 */

//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================

native W3FileBugReport(client, String:reportstr[]);