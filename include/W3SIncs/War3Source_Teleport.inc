/**
 * File: War3Source_Teleport.inc
 * Description: Stocks regarding teleport
 * Author(s): thorgot
 */

#if defined __war3source_teleport__
#endinput
#endif
#define __war3source_teleport__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================


native bool:W3Teleport(client,Float:distance,Float:resistFallDamageTime=3.0);
native bool:W3Blink(client,Float:distance,skill,bool:doBeam=false,r=0,g=0,b=0,bool:useMovingDirection=false,bool:ignoreImmunity=false,bool:usesound=true);
