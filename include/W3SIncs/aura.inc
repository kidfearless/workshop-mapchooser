///AURA SYSTEM, keeps track of players aura via distance
//This function registers an aura, returns an number, save it as your AuraID
//by default tracks teammates only
//Track other team will track other team only, if you want to track both teams, use two different auras
//aura not does take account of line of sight
native W3RegisterAura(String:auraShortName[],Float:distance,bool:trackotherteam=false);

//set and unset a player if he has or no longer has the origin of the aura
//removed if player disconnects
//level (optional) is a integer for a priority system, such as teammate has level 1 heal wave and another teammate has level 4 heal wave
//the player recieves the level 4 heal wave and does not stack with level 1 heal wave
native W3SetAuraFromPlayer(tAuraID,client,bool:auraOriginatesFromPlayer=true,level=1,bool:enable=true);

///is this player under the influence of this aura?
///it means player is within distance of an aura origin (ie a player)
///level of the aura recieved is passed by reference and will be set
native bool:W3HasAura(tAuraID,client,&level);

//forward when a player's aura state changes, ie leaves the area, or dies
//however player may be dead while the aura lingers, always check for alive in your function
forward OnW3PlayerAuraStateChanged(client,tAuraID,bool:inAura,level);

//adds auras
//if distance changes w/ skill level I suggest W3RemoveAura then W3AddAura to reset the distance
//skillid 0 for items
//returns AuraID (int 0 to 99)
native W3AddAura(client,raceoritemid,skillid=0,Distance,bool:trackenemies=true,bool:trackallies=false);
//lol take a guess what it does, auraid is returned from W3AddAura
native W3RemoveAura(auraid);
//client refers to other players but could theoretically also be the owner of the aura
//tracks players based on the bools in W3AddAura - will not track allies or enemies otherwise
native bool:W3InAura(client,auraid);

forward OnW3AuraStateChanged(client,owner,auraid,bool:inAura,level);
//only ticks if inaura obv
//always use aurastate changed as well to remove effects
forward OnW3AuraTick(client,owner,auraid,level);
