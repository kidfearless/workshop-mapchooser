/**
 * File: War3Source_RaceRestrictions.inc
 * Description: Stocks regarding Races
 * Author(s): thorgot
 */

#if defined __war3source_racerestrictions__
#endinput
#endif
#define __war3source_racerestrictions__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================

native W3CanChooseRace(client, race_selected, bool:printErrors);
native W3IsRestrictedRace(client, race_selected, bool:printErrors);
native W3RaceTeamLimitReached(client, race_selected, bool:printErrors);