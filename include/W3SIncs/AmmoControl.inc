/**
 * File: AmmoControl.inc
 * Description: Native Interface to Ammo Control Addon.
 * Author(s): Frenzzy
 * 
 *
 * THIS FILE MUST BE MANUALLY INCLUDED   
 */

/**
 * Give ammo and/or clip for player's weapon.
 *
 * @param client: Client Index. Must be valid.
 * @param weapon: Weapon Index. Must be valid.
 * @param ammo: Amount of Ammo. Optional value. Use -1 to leave as it is.
 * @param clip: Amount of Clip. Optional value. Use -1 to leave as it is.
 * @noreturn
 */
native War3_GiveWeaponAmmo(client, weapon, ammo = -1, clip = -1);

/**
 * Set Ammo Control for player.
 * Auto control for the specified amount of ammo and the ammo in the clip after a buy (rebuy and autobuy) or reload.
 * Does not set an ammo and a clip, when GivePlayerItem used. Changes an ammo and/or a clip with a War3_GiveWeaponAmmo after a GivePlayerItem.
 * Also it does not give ammo, when player picks-up and gets the specified weapon from another player.
 *
 * @param client: Client Index. Use only this parameter to reset the ammo and the clip to deafult.
 * @param weapon: Weapon Name. Optional value. You can use these values instead of weapon names: weapon_primary, weapon_secondary, weapon_grenade or weapon_all.
 * @param ammo: Amount of Ammo. Optional value. Use -1 to leave as it is.
 * @param clip: Amount of Clip. Optional value. Use -1 to leave as it is.
 * @param update: Immediately Update. Optional value. Immediately ammo and/or clip update, if player is alive and has specified weapon(s).
 * @noreturn
 */
native War3_SetAmmoControl(client, String:weapon[], ammo = -1, clip = -1, bool:update = false);

/**
 * Set Max Ammo that a weapon within a specific slot will reload to for a client.
 *
 * @param client is client index
 * @param weapon string is weapon_ak47 etc
 * @param slot is 0 for primary or 1 for secondary
 * @param ammo is the ammo the weapon will reload to, -1 resets it to default
 */

native War3_SetMaxAmmo(client, String:weapon[], slot, ammo);

/**
 * Set Max Ammo that a weapon within a specific slot will reload to for a client.
 *
 * @param client is client index
 * @param weapon string is weapon_ak47 etc
 * @param slot is 0 for primary or 1 for secondary
 * @return ammo value set for gun or -1 if you should use max ammo for the gun instead
 */

native War3_GetMaxAmmo(client, String:weapon[], slot);