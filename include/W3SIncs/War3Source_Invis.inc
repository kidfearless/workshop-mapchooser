/**
 * File: War3Source_Invis.inc
 * Description: Stocks regarding Invis
 * Author(s): thorgot
 */

#if defined __war3source_invis__
#endinput
#endif
#define __war3source_invis__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================


native bool:W3GetForceInvisModelRefresh(client);
native bool:W3SetForceInvisModelRefresh(client, bool:refresh);
native W3Fadein(client, Float:time_til_normal, Float:normalAlpha, Float:startingAlpha, num_Chunks=5);
native bool:W3ShouldRestartFade(client);
native W3SetCustomModel(client, bool:enableCustomModel,String:ModelName[],bool:onSpawn);
native W3HasCustomModel(client);
native W3CreatePlayerModel(client, Float:ModelDistance, Float:invis=1.0, bool:ModelSolidity=false, Float:duration=10.0);