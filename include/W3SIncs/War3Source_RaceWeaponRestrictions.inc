/**
 * File: War3Source_RaceWeaponRestrictions.inc
 * Description: Stocks regarding Races
 * Author(s): thorgot
 */

#if defined __war3source_raceweaponrestrictions__
#endinput
#endif
#define __war3source_raceweaponrestrictions__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================


native W3GetRaceCanUsePistols(race);
native W3GetRaceCanUsePrimaries(race);
native W3GetRaceCanUseGrenades(race);
native W3GetRaceCanUseKnives(race);
native W3SetRaceCanUsePistols(race, canUsePistols_arg);
native W3SetRaceCanUsePrimaries(race, canUsePrimaries_arg);
native W3SetRaceCanUseGrenades(race, canUseGrenades_arg);
native W3SetRaceCanUseKnives(race, canUseKnives_arg);
native W3GetRaceOneWeaponRace(race);
native W3GetRaceUniqueWeapon(race, String:uniqueWeapon[], len);
native W3SetRaceOnePistolRace(race, String:singlePistol_arg[]);
native W3SetRaceOnePrimaryRace(race, String:singlePrimary_arg[]);
native W3SetPlayerCanUseSpecialWeapon(client, bool:canUseSpecialWeapon);
native W3SetRaceCustomWeaponString(race, String:display_str[]);
native W3GetRaceCustomWeaponString(race, String:display_str[], len);
native W3SetRaceCustomWeaponSymbol(race, String:display_str[]);
native W3GetRaceWeaponSymbol(race, String:display_str[], len);
native W3SwitchClientToWeaponSlot(client, slot);
native W3DropOrKillPistol(client);
native bool:W3DropWeaponSlot(client, slot, bool:blockable=true);
native W3DropWeaponSlotDelayed(client, slot, bool:blockable=true);
native W3ResetRaceWeaponRestrictions(race);
native W3GiveSpecialWeapon(client,String:weaponname[],bool:switchTo=false,reserveammo=-1,clipammo=-1,Float:delay=0.1,bool:secondary=false);
native W3KillSpecialWeapon(client, bool:secondary = false);

/**
 * Can the client use the given weapon (which must be prepended with weapon_)
 */
native W3CanUseWeapon(client, String:weapon_name[]);

/**
 * Can the client use any weapon.
 * Only to be used while giving a specific weapon the client wouldn't ordinarily be able to pick up.
 * For example, Siege Tank can only use the awp or mag7 he is given, no other.
 */
native W3CanUseAnyWeapon(client);