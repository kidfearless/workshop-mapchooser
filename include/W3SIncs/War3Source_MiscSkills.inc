/**
 * File: War3Source_MiscSkills.inc
 * Description: Stocks regarding Miscellaneous Skills
 * Author(s): thorgot
 */

#if defined __war3source_miscskills__
#endinput
#endif
#define __war3source_miscskills__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================

native W3GetMoney(client);
native W3SetMoney(client, amt);
native W3Knockback(victim, attacker, Float:force, bool:KnockForward=false, Float:KnockUpForce=350.0);
native W3Poison(victim, attacker, damage, hits, Float:interval);
native W3Slow(victim,attacker,raceid,Float:speedpct,Float:duration,bool:Use_fSlow2=false);
native bool:W3PlaceTripMine(client);
native bool:W3PlacePoisonMine(client);
native W3GiveMissile(client,String:projectile_type[],maxmissiles=1,cost=0);
native W3TooCloseToBomb(client,Float:distance,bool:printhint);
//returns true if round is live, false if round is not live
native bool:War3_RoundLive();
native bool:W3IsSupp(client);
//if limit isn't set, it is limited by War3_GetMaxHP(client), returns true if healed, false if unable to heal or if health is above limit
native bool:War3_Heal(client,amount,limit=-1);
native W3SuppCount();
native AFKCheck(client = 0);
forward OnWar3EventHeal(client,healamt,limit);