/**
 * File: War3Source_UltDamage.inc
 * Description: Stocks regarding Ultimates that do damage
 * Author(s): thorgot
 */

#if defined __war3source_ultdamage__
#endinput
#endif
#define __war3source_ultdamage__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================


native bool:W3DoUltDamage(client,Float:maxdistance,dmg,String:weaponname[],String:skillname[], bool:doBeam, bool:doBloodOnTarget, any:soundNumber);

enum
{
    ULTDAMAGE_NOSOUND=0,
    ULTDAMAGE_LIGHTNING,
	ULTDAMAGE_NUMSOUNDS	//Must be the last value. Used to size things appropriately.
}