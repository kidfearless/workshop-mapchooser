/**
 * File: War3Source_Races.inc
 * Description: Stocks regarding Races
 * Author(s): War3Source Team  
 */

//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================

//creates a new race, returns the race id
//a valid race is >0
native War3_CreateNewRace(String:name[],String:shortname[],reload_race_id=0);

//adds a skill or a ultimate
native War3_AddRaceSkill(raceid,String:tskillorultname[],String:tskillorultdescription[],bool:isult=false,maxskilllevel=DEF_MAX_SKILL_LEVEL);

//translated
//creates a new race, returns the race id
native War3_CreateNewRaceT(String:shortname[],reload_race_id=0);

//translated
//adds a skill or a ultimate
//additional parameters replaces #1# #2# ... in the translation string (max 5)
// % does not need to be escaped
native War3_AddRaceSkillT(raceid,String:SkillNameIdentifier[],bool:isult=false,maxskilllevel=DEF_MAX_SKILL_LEVEL,any:...);

//ends race creation!!! MUST CALL THIS!!!
native War3_CreateRaceEnd(raceid);

native War3_GetRaceName(raceid,String:retstr[],maxlen);
native War3_SetRaceName(raceid,String:newname[],maxlen);
native War3_GetRaceShortname(raceid,String:retstr[],maxlen);

native War3_GetRaceCustomName(client,raceid,String:custname[],maxlen);
native War3_SetRaceCustomName(client,raceid,String:custname[]);

#pragma deprecated Not Fully Implemented, possibly in the future
native W3GetRaceString(raceid,RaceString:property,String:retstr[],maxlen);
#pragma deprecated Not Fully Implemented, possibly in the future
native W3GetRaceSkillString(raceid,skillnum,SkillString:property,String:retstr[],maxlen);

native War3_CreateGenericSkill(String:gskillname[]);
//genericSkillData is an array or trie that modifies behavior of that generic skill
//if NEW HANDLE is passed, OLD one will be closed (i e during create race call after race has been created like on a map change)
//this means you CAN recreate new handles and pass them, the old one will be closed
native War3_UseGenericSkill(raceid,String:gskillname[],Handle:genericSkillData,String:yourskillname[],String:untranslatedSkillDescription[]="ERR: No Skill Description Entered.",bool:translated=false,bool:isUltimate=false,maxskilllevel=DEF_MAX_SKILL_LEVEL,any:...);

native War3_GetRacesLoaded();
native W3GetRaceMaxLevel(race);
native W3SetRaceMaxLevel(race, maxlevel);

//Disables the spend skills menu on a race
native W3SetDisableSpendSkillsMenu(race);
native W3GetDisableSpendSkillsMenu(race);

//checks if a raceid is being played, true if it is, false if it doesn't exist or isn't being played
native W3RaceEnabled(race);

native War3_IsSkillUltimate(raceid, skillnum);
native War3_GetRaceSkillCount(raceid);
native W3GetRaceSkillName(raceid,skillindex,String:retstr[],maxlen);
native W3GetRaceSkillDesc(raceid,skillindex,String:retstr[],maxlen);
native W3GetRaceSkillMaxLevel(raceid, skillnum);
native W3GetRaceOrder(raceid);
//Flag is a generic string. We compare the race_flags cvar to the string you passed
native bool:W3RaceHasFlag(raceid,String:flag[]);

//returns a simple array of race IDs, sorted properly.
//Does not include "hidden" races
//pass array of MAXRACES, returns number of races in the array. starts at 0
//ordered properly
//returns the size of the array
native W3GetRaceList(racelist[]);

//a valid race is >0
native War3_GetRaceIDByShortname(String:raceshortname[]);

native W3GetRaceAccessFlagStr(raceid,String:ret[],maxlen);
native W3GetRaceItemRestrictionsStr(raceid,String:ret[],maxlen);
native W3GetRaceMaxLimitTeam(raceid, team);
native W3GetRaceMaxLimitTeamCvar(raceid, team); //returns the internal cvar id (int not handle)
native W3GetRaceMinLevelRequired(raceid);
native W3GetRaceMinGoldRequired(raceid);
native W3IsRaceTranslated(raceid);

native W3GetRaceCell(raceid,ENUM_RaceObject:property);
native W3SetRaceCell(raceid,ENUM_RaceObject:property,any:somevalue);

native Handle:W3GetSpecificRaceList(client);
native W3GetSpecificRaceListCount(client);
native W3SetSpecificRaceListCount(client, count);

//use W3GetMinUltLevel() for ultmin
//checks if you can level a skill up based on the current skill requirements formula
//DO NOT CHANGE THIS FORMULA WITHOUT CHANGING THE ONE BELOW FOR W3MAXSKILLLEVEL!!!
//PLEASE
stock bool:W3CanLevel(level,currentskilllevel,bool:isultskill,ultmin)
{
    if(!isultskill)
    {
        return level>=currentskilllevel*2+1;
    }
    else
    {
        return level>=currentskilllevel*2+1+ultmin-1;
    }
}

//use W3GetMinUltLevel() for ultmin
//returns the maximum a skill can be leveled to at a current level
//DO NOT CHANGE THIS FORMULA WITHOUT CHANGING THE ONE ABOVE FOR W3CANLEVEL!!!
//PLEASE
stock W3MaxSkillLevel(level,bool:isultskill,ultmin)
{
    if(!isultskill)
    {
	new maxlvl = RoundToFloor(float(level+1)/(2.0));
        return maxlvl;
    }
    else
    {
	new maxlvl = RoundToFloor((float(ultmin-2-level))/(-2.0));
        return maxlvl;
    }
}

//=============================================================================
// RACE Forwards
//=============================================================================
forward OnWar3RaceEnabled(newrace);
forward OnWar3RaceDisabled(oldrace);

// Makes it so that races are reloadable
// You need to use OnWar3SmartLoadRaceOrItemOrdered in your race instead of the
// usual OnWar3LoadRaceOrItemOrdered or OnWar3LoadRaceOrItemOrdered2.
native War3_RaceOnPluginStart(String:shortname[16]);
native War3_RaceOnPluginEnd(String:shortname[16]);
native bool:War3_IsRaceReloading();
