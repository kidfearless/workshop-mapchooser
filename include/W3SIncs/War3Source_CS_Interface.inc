/**
 * File: War3Source_CS_Interface.inc
 * Description: Functions and stuff to make CS specific races and whatnot
 * Author(s): War3Source Team  
 */

native IsSilencedWeapon(weapon);
native IsCZ75A(weapon);

/**
 * Changes a clients armor value.
 *
 * @param client      Client index
 * @param amount      Armor value
 * @noreturn
 */
stock War3_SetCSArmor(client, amount) {
    // Revan: thoose armor properities for CS:GO are the same as in CS:Source
    new ValveGameEnum:war3Game = War3_GetGame();
    if (war3Game == Game_CS || war3Game == Game_CSGO) {
        if (amount > 250) {
            amount = 250;
        }
        SetEntProp(client, Prop_Send, "m_ArmorValue", amount);
    }
}

/**
 * Retrieves a clients armor value.
 *
 * @param client      Client index
 * @return              Armor value
 */
stock War3_GetCSArmor(client) {
    new ValveGameEnum:war3Game = War3_GetGame();
    if (war3Game == Game_CS || war3Game == Game_CSGO) {
        return GetEntProp(client, Prop_Send, "m_ArmorValue");
    }
    return 0;
}

/**
 * Returns whether the client is currently wearing a helmet or not.
 *
 * @param client      Client index
 * @return              True if client is wearing a helmet
 */
stock bool:War3_GetCSArmorHasHelmet(client) {
    return bool:GetEntProp(client, Prop_Send, "m_bHasHelmet");
}

/**
 * Gives or removes a helmet from the player.
 *
 * @param client      Client index
 * @param hashelmet   True to give helmet, false to remove
 * @noreturn
 */
stock War3_SetCSArmorHasHelmet(client,bool:hashelmet) {
    return SetEntProp(client, Prop_Send, "m_bHasHelmet",hashelmet? 1:0, 1);
}

/**
 * Restores players armor and helmet.
 *
 * @param client        Client index
 * @noreturn
 */
native War3_RestoreCachedCSArmor(client);

native bool:War3_IsHoldingHostage(client);

stock War3_GetRoundNumber() {
    return (GetTeamScore(2) + GetTeamScore(3)) + 1;
}

new const String:CLIP_HUNDRED_FIFTY_NAMES[] = "weapon_negev";
new const String:CLIP_HUNDRED_NAMES[] = "weapon_m249";
new const String:CLIP_SIXTY_FOUR_NAMES[] = "weapon_bizon";
new const String:CLIP_FIFTY_NAMES[] = "weapon_p90";
new const String:CLIP_THIRTY_FIVE_NAMES[] = "weapon_galilar";
new const String:CLIP_THIRTY_TWO_NAMES[] = "weapon_tec9";
new const String:CLIP_THIRTY_NAMES[] = "weapon_ak47,weapon_m4a1,weapon_mac10,weapon_mp7,weapon_mp9,weapon_sg556,weapon_aug,weapon_elite";
new const String:CLIP_TWENTY_FIVE_NAMES[] = "weapon_ump45,weapon_famas";
new const String:CLIP_TWENTY_NAMES[] = "weapon_g3sg1,weapon_m4a1_silencer,weapon_scar20,weapon_glock,weapon_fiveseven";
new const String:CLIP_THIRTEEN_NAMES[] = "weapon_p250,weapon_hkp2000";
new const String:CLIP_TWELVE_NAMES[] = "weapon_usp_silencer,weapon_cz75a";
new const String:CLIP_TEN_NAMES[] = "weapon_awp,weapon_ssg08";
new const String:CLIP_EIGHT_NAMES[] = "weapon_nova,weapon_revolver";
new const String:CLIP_SEVEN_NAMES[] = "weapon_sawedoff,weapon_xm1014,weapon_deagle";
new const String:CLIP_FIVE_NAMES[] = "weapon_mag7";

//Gets max clip size for the weapon
//Only filled in for primaries above, fill it in for pistols if you want more.
stock GetWeaponMaxAmmoEx(weapon) {
	if (!IsValidEntity(weapon))
		return -1;
	new String:S_Weaponname[32];
	Weapon_GetName(weapon,S_Weaponname,32);
	return GetWeaponMaxAmmo(S_Weaponname);
}

stock GetWeaponMaxAmmo(String:weapon_name[]) {
	if(StrContains(CLIP_THIRTY_NAMES, weapon_name) != -1) {
		return 30; //Must be first because weapon_m4a1_silencer contains weapon_m4a1
	}
	else if(StrContains(CLIP_TWENTY_NAMES, weapon_name) != -1)
		return 20;
	else if(StrContains(CLIP_THIRTY_FIVE_NAMES, weapon_name) != -1)
		return 35;
	else if(StrContains(CLIP_FIFTY_NAMES, weapon_name) != -1)
		return 50;
	else if(StrContains(CLIP_SIXTY_FOUR_NAMES, weapon_name) != -1)
		return 64;
	else if(StrContains(CLIP_HUNDRED_FIFTY_NAMES, weapon_name) != -1)
		return 150;
	else if(StrContains(CLIP_TWENTY_FIVE_NAMES, weapon_name) != -1)
		return 25;
	else if(StrContains(CLIP_TEN_NAMES, weapon_name) != -1)
		return 10;
	else if(StrContains(CLIP_EIGHT_NAMES, weapon_name) != -1)
		return 8;
	else if(StrContains(CLIP_SEVEN_NAMES, weapon_name) != -1)
		return 7;
	else if(StrContains(CLIP_FIVE_NAMES, weapon_name) != -1)
		return 5;
	else if(StrContains(CLIP_HUNDRED_NAMES, weapon_name) != -1)
		return 100;
	else if(StrContains(CLIP_THIRTEEN_NAMES, weapon_name) != -1)
		return 13;
	else if(StrContains(CLIP_TWELVE_NAMES, weapon_name) != -1)
		return 12;
	else if(StrContains(CLIP_THIRTY_TWO_NAMES, weapon_name) != -1)
		return 32;
	else {
		return -1;
	}
}
