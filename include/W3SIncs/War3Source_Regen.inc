/**
 * File: War3Source_Regen.inc
 * Description: Stocks regarding Regen
 * Author(s): thorgot
 */

#if defined __war3source_regen__
#endinput
#endif
#define __war3source_regen__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================


native W3CreateGroupRegen(client, health, Float:delay, Float:distance);
native W3StopGroupRegen(client);