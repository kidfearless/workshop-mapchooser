/**
 * File: War3Source_Props.inc
 * Description: Stocks regarding spawning props, free-roaming or attached to players
 * Author(s): thorgot
 */

#if defined __war3source_props__
#endinput
#endif
#define __war3source_props__
 
//=======================================================================
//                             NATIVE / STOCKS
//=======================================================================


native W3SpawnAttachedProp(client, String:propmodel[], Float:angles[], Float:shift_position[], model_index, Float:fadedistance);
native W3KillAttachedProps(client);
native W3GetPropSpawnPosition(client,Float:MaxDistance, Float:position[]);
native W3SpawnProp(Float:Origin[], Float:Direction[], String:Model[], bool:explosive);
