#if defined _funcommands_included
 #endinput
#endif
#define _funcommands_included
/*
	Freezes a client for the default duration.
*/
native int Freeze_Client(int client);