/*
 * ============================================================================
 *
 *  Zombie:Reloaded
 *
 *  File:		  zmarket_cmds.inc
 *  Type:		  Module
 *  Description:   ZMarket Commands module, allows players to type in chat for weapons.
 *
 *  Copyright (C) 2017 AntiTeal
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ============================================================================
 */

#define ZMARKET_CMDS_REBUY false

 ZMarketCMDsOnCommandsCreate()
 {
	 //Oh boy, here it comes!
 	//Grenades + Misc Items
 	RegConsoleCmd("sm_decoy", Command_Decoy);
 	RegConsoleCmd("sm_he", Command_HE);
	RegConsoleCmd("sm_smoke", Command_Smoke);
	RegConsoleCmd("sm_flash", Command_Flash);
	RegConsoleCmd("sm_flashbang", Command_Flash);
 	RegConsoleCmd("sm_molotov", Command_Molotov);
 	RegConsoleCmd("sm_incendiary", Command_Incendiary);
	RegConsoleCmd("sm_inc", Command_Incendiary);
	RegConsoleCmd("sm_incgrenade", Command_Incendiary);
 	RegConsoleCmd("sm_kevlar", Command_Kevlar);
 	RegConsoleCmd("sm_knife", Command_Knife);
 	//Pistols
 	RegConsoleCmd("sm_glock", Command_Glock);
 	RegConsoleCmd("sm_glock18", Command_Glock);
 	RegConsoleCmd("sm_cz75", Command_CZ75);
 	RegConsoleCmd("sm_cz75a", Command_CZ75);
 	RegConsoleCmd("sm_cz", Command_CZ75);
 	RegConsoleCmd("sm_usps", Command_USP);
 	RegConsoleCmd("sm_usp", Command_USP);
 	RegConsoleCmd("sm_p2000", Command_P2000);
	RegConsoleCmd("sm_p2k", Command_P2000);
 	RegConsoleCmd("sm_tec", Command_Tec9);
 	RegConsoleCmd("sm_tec9", Command_Tec9);
 	RegConsoleCmd("sm_p250", Command_P250);
 	RegConsoleCmd("sm_deagle", Command_Deagle);
 	RegConsoleCmd("sm_deag", Command_Deagle);
 	RegConsoleCmd("sm_deserteagle", Command_Deagle);
 	RegConsoleCmd("sm_elite", Command_Elite);
 	RegConsoleCmd("sm_dual", Command_Elite);
 	RegConsoleCmd("sm_berettas", Command_Elite);
 	RegConsoleCmd("sm_dualberettas", Command_Elite);
 	RegConsoleCmd("sm_fiveseven", Command_FiveSeven);
 	RegConsoleCmd("sm_57", Command_FiveSeven);
 	RegConsoleCmd("sm_revolver", Command_Revolver);
 	RegConsoleCmd("sm_r8", Command_Revolver);
 	//SMGs
 	RegConsoleCmd("sm_mac", Command_Mac10);
 	RegConsoleCmd("sm_mac10", Command_Mac10);
 	RegConsoleCmd("sm_mp9", Command_MP9);
 	RegConsoleCmd("sm_pp", Command_PPBizon);
 	RegConsoleCmd("sm_bizon", Command_PPBizon);
 	RegConsoleCmd("sm_ppbizon", Command_PPBizon);
 	RegConsoleCmd("sm_mp7", Command_MP7);
 	RegConsoleCmd("sm_ump", Command_UMP45);
 	RegConsoleCmd("sm_ump45", Command_UMP45);
 	RegConsoleCmd("sm_p90", Command_P90);
 	//Rifles
 	RegConsoleCmd("sm_galil", Command_GalilAR);
 	RegConsoleCmd("sm_galilar", Command_GalilAR);
 	RegConsoleCmd("sm_famas", Command_Famas);
 	RegConsoleCmd("sm_ak", Command_AK47);
 	RegConsoleCmd("sm_ak47", Command_AK47);
 	RegConsoleCmd("sm_m4", Command_M4A4);
 	RegConsoleCmd("sm_m4a4", Command_M4A4);
 	RegConsoleCmd("sm_m4a1", Command_M4A1s);
 	RegConsoleCmd("sm_m4a1s", Command_M4A1s);
 	RegConsoleCmd("sm_scar", Command_Scar20);
 	RegConsoleCmd("sm_scar20", Command_Scar20);
 	RegConsoleCmd("sm_sg", Command_SG553);
 	RegConsoleCmd("sm_sg553", Command_SG553);
 	RegConsoleCmd("sm_aug", Command_AUG);
 	RegConsoleCmd("sm_ssg", Command_SSG08);
 	RegConsoleCmd("sm_ssg08", Command_SSG08);
 	RegConsoleCmd("sm_g3sg1", Command_G3SG1);
 	RegConsoleCmd("sm_awp", Command_AWP);
 	//Heavy
 	RegConsoleCmd("sm_negev", Command_Negev);
 	RegConsoleCmd("sm_m249", Command_M249);
 	RegConsoleCmd("sm_nova", Command_Nova);
 	RegConsoleCmd("sm_xm", Command_XM1014);
 	RegConsoleCmd("sm_xm1014", Command_XM1014);
 	RegConsoleCmd("sm_sawed", Command_SawedOff);
 	RegConsoleCmd("sm_sawedoff", Command_SawedOff);
 	RegConsoleCmd("sm_mag", Command_Mag7);
 	RegConsoleCmd("sm_mag7", Command_Mag7);
 }

 //Oh boy, here it comes (Part 2)
 public Action:Command_Smoke(int client, int args)
 {
 	ZMarketEquip(client, "Smokegrenade", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Flash(int client, int args)
 {
 	ZMarketEquip(client, "Flashbang", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Decoy(int client, int args)
 {
 	ZMarketEquip(client, "Decoy", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_HE(int client, int args)
 {
 	ZMarketEquip(client, "HEGrenade", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Molotov(int client, int args)
 {
 	ZMarketEquip(client, "Molotov", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Incendiary(int client, int args)
 {
 	ZMarketEquip(client, "IncGrenade", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Kevlar(int client, int args)
 {
 	ZMarketEquip(client, "Kevlar Vest", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Knife(int client, int args)
 {
 	ZMarketEquip(client, "Knife", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Glock(int client, int args)
 {
 	ZMarketEquip(client, "Glock", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_CZ75(int client, int args)
 {
 	ZMarketEquip(client, "CZ75-Auto", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_USP(int client, int args)
 {
 	ZMarketEquip(client, "USP-S", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_P2000(int client, int args)
 {
 	ZMarketEquip(client, "P2000", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Tec9(int client, int args)
 {
 	ZMarketEquip(client, "Tec-9", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_P250(int client, int args)
 {
 	ZMarketEquip(client, "P250", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Deagle(int client, int args)
 {
 	ZMarketEquip(client, "Deagle", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Elite(int client, int args)
 {
 	ZMarketEquip(client, "Elite", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_FiveSeven(int client, int args)
 {
 	ZMarketEquip(client, "Fiveseven", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Revolver(int client, int args)
 {
 	ZMarketEquip(client, "R8 Revolver", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Mac10(int client, int args)
 {
 	ZMarketEquip(client, "Mac10", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_MP9(int client, int args)
 {
 	ZMarketEquip(client, "MP9", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_PPBizon(int client, int args)
 {
 	ZMarketEquip(client, "Bizon", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_MP7(int client, int args)
 {
 	ZMarketEquip(client, "MP7", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_UMP45(int client, int args)
 {
 	ZMarketEquip(client, "UMP45", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_P90(int client, int args)
 {
 	ZMarketEquip(client, "P90", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_GalilAR(int client, int args)
 {
 	ZMarketEquip(client, "Galil AR", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Famas(int client, int args)
 {
 	ZMarketEquip(client, "Famas", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_AK47(int client, int args)
 {
 	ZMarketEquip(client, "AK47", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_M4A4(int client, int args)
 {
 	ZMarketEquip(client, "M4A4", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_M4A1s(int client, int args)
 {
 	ZMarketEquip(client, "M4A1-S", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Scar20(int client, int args)
 {
 	ZMarketEquip(client, "SCAR-20", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_SG553(int client, int args)
 {
 	ZMarketEquip(client, "SG556", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_AUG(int client, int args)
 {
 	ZMarketEquip(client, "AUG", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_SSG08(int client, int args)
 {
 	ZMarketEquip(client, "SSG 08", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_G3SG1(int client, int args)
 {
 	ZMarketEquip(client, "G3SG1", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_AWP(int client, int args)
 {
 	ZMarketEquip(client, "AWP", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Negev(int client, int args)
 {
 	ZMarketEquip(client, "Negev", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_M249(int client, int args)
 {
 	ZMarketEquip(client, "M249", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Nova(int client, int args)
 {
 	ZMarketEquip(client, "Nova", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_XM1014(int client, int args)
 {
 	ZMarketEquip(client, "XM1014", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_SawedOff(int client, int args)
 {
 	ZMarketEquip(client, "Sawed-Off", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
 public Action:Command_Mag7(int client, int args)
 {
 	ZMarketEquip(client, "Mag-7", ZMARKET_CMDS_REBUY);
 	return Plugin_Handled;
 }
